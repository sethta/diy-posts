(function( $ ) {
  'use strict';

// Color picker
$('.color-picker').wpColorPicker();

// Submit spinner
$( '.submit #submit, .submit .button-primary' ).click( function() {
  $(this).parent('.submit').next('.spinner').addClass('is-active');
});

})( jQuery );
