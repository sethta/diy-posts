<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://sethalling.com/
 * @since      1.0.0
 *
 * @package    DIY_Posts
 * @subpackage DIY_Posts/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    DIY_Posts
 * @subpackage DIY_Posts/admin
 * @author     Seth Alling <seth@sethalling.com>
 */
class DIY_Posts_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * The plugin options.
	 *
	 * @since   1.0.0
	 * @access   private
	 * @var   string   	$options    The plugin options.
	 */
	private $options;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param    string    $plugin_name    The name of this plugin.
	 * @param    string    $version        The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

		$this->get_options();

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since   1.0.0
	 */
	public function enqueue_styles() {

		global $pagenow;

		if ( 'options-general.php' == $pagenow && ! empty( $_GET['page'] ) && 'diy-posts-settings' == $_GET['page'] ) {

			wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/diy-posts-admin-settings.css', array(), $this->version, 'all' );
			wp_enqueue_style( 'wp-color-picker' );

		}

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since   1.0.0
	 */
	public function enqueue_scripts() {

		global $pagenow;

		if ( 'options-general.php' == $pagenow && ! empty( $_GET['page'] ) && 'diy-posts-settings' == $_GET['page'] ) {

			wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/diy-posts-admin-settings.js', array( 'jquery', 'wp-color-picker' ), $this->version, true );

			wp_enqueue_script( 'wp-color-picker' );

		}

	}

	/**
	 * Add settings page to options sidebar
	 *
	 * @link     https://codex.wordpress.org/Administration_Menus
	 * @since    1.0.0
	 * @return   void
	 */
	public function add_menu() {

		add_options_page(
			__( 'DIY Posts Settings', $this->plugin_name ),
			__( 'DIY Posts', $this->plugin_name ),
			'manage_options',
			$this->plugin_name . '-settings',
			array( $this, 'page_settings' )
		);

	}

	/**
	 * Create the settings page output
	 *
	 * @since    1.0.0
	 * @return   void
	 */
	public function page_settings() {

		include( plugin_dir_path( __FILE__ ) . 'partials/diy-posts-admin-page-settings.php' );

	}

	/**
	 * Register settings fields with WordPress
	 *
	 * @since    1.0.0
	 * @return   void
	 */
	public function register_fields() {

		// add_settings_field( $id, $title, $callback, $menu_slug, $section, $args );

		add_settings_field(
			'background-color',
			apply_filters( $this->plugin_name . 'label-background-color', esc_html__( 'Background Color', $this->plugin_name ) ),
			array( $this, 'field_color' ),
			$this->plugin_name,
			$this->plugin_name . '-settings',
			array(
				'description' => 'The color of the DIY info background',
				'id'          => 'background-color',
				'value'       => '#cccccc',
			)
		);

	}

	/**
	 * Returns an array of options names and fields types
	 *
	 * @return   array   An array of options
	 */
	public static function get_options_list() {

		$options = array(
			array( 'background-color', 'color' ),
		);

		return $options;

	}

	/**
	 * Register settings sections with WordPress
	 *
	 * @since    1.0.0
	 * @return   void
	 */
	public function register_sections() {

		// add_settings_section( $id, $title, $callback, $menu_slug );

		add_settings_section(
			$this->plugin_name . '-settings',
			apply_filters( $this->plugin_name . 'section-title-settings', null ),
			array( $this, 'section_settings' ),
			$this->plugin_name
		);

	}

	/**
	 * Create settings section
	 *
	 * @since    1.0.0
	 * @param    array   $params   Array of parameters for the section
	 * @return   mixed             The settings section
	 */
	public function section_settings( $params ) {

		include( plugin_dir_path( __FILE__ ) . 'partials/' . $this->plugin_name . '-admin-section-settings.php' );

	}

	/**
	 * Register plugin settings
	 *
	 * @since    1.0.0
	 * @return   void
	 */
	public function register_settings() {

		// register_setting( $option_group, $option_name, $sanitize_callback );

		register_setting(
			$this->plugin_name . '-settings',
			$this->plugin_name . '-settings',
			array( $this, 'validate_options' )
		);

	}

	/**
	 * Gets the class variable $options
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function get_options() {

		$this->options = get_option( $this->plugin_name . '-settings' );

	}

	/**
	 * Creates a text field
	 *
	 * @param 	array    $args   The arguments for the field
	 * @return 	string           The HTML field
	 */
	public function field_text( $args ) {

		$defaults = array(
			'class' => 'text widefat',
			'description' => '',
			'label' => '',
			'name' => $this->plugin_name . '-settings[' . $args['id'] . ']',
			'placeholder' => '',
			'type' => 'text',
			'value' => '',
		);

		$defaults = apply_filters( $this->plugin_name . '-field-text-defaults', $defaults );

		$atts = wp_parse_args( $args, $defaults );

		if ( ! empty( $this->options[ $atts['id'] ] ) ) {

			$atts['value'] = $this->options[ $atts['id'] ];

		}

		include( plugin_dir_path( __FILE__ ) . 'partials/' . $this->plugin_name . '-admin-field-text.php' );

	}

	/**
	 * Creates a color field
	 *
	 * @param 	array    $args   The arguments for the field
	 * @return 	string           The HTML field
	 */
	public function field_color( $args ) {

		$defaults = array(
			'class' => 'text widefat color-picker',
			'description' => '',
			'label' => '',
			'name' => $this->plugin_name . '-settings[' . $args['id'] . ']',
			'placeholder' => '',
			'type' => 'text',
			'value' => '',
		);

		$defaults = apply_filters( $this->plugin_name . '-field-color-defaults', $defaults );

		$atts = wp_parse_args( $args, $defaults );

		if ( ! empty( $this->options[ $atts['id'] ] ) ) {

			$atts['value'] = $this->options[ $atts['id'] ];

		}

		include( plugin_dir_path( __FILE__ ) . 'partials/' . $this->plugin_name . '-admin-field-text.php' );

	}

	/**
	 * Validates saved options
	 *
	 * @since    1.0.0
	 * @param    array   $input   array of submitted plugin options
	 * @return   array            array of validated plugin options
	 */
	public function validate_options( $input ) {

		$valid = array();
		$options = $this->get_options_list();

		foreach ( $options as $option ) {

			$name = $option[0];
			$type = $option[1];

			if ( 'text' == $type ) {

				$valid[ $option[0] ] = sanitize_text_field( $input[ $name ] );

			} elseif ( 'color' == $type ) {

				$color = strip_tags( stripslashes( $input[ $name ] ) );

				if ( false == $this->check_color( $color ) ) {

					// Set the error message
					add_settings_error( $this->plugin_name . '-settings', 'bg_error', 'Insert a valid color', 'error' );

					// Get the previous valid value
					$valid[ $option[0] ] = $this->options[ $option[0] ];

				} else {

					$valid[ $option[0] ] = $color;

				}

			} else {

				$valid[ $option[0] ] = $input[ $name ];

			}

		}

		return $valid;

	}

	/**
	 * Function that will check if value is a valid HEX color.
	 *
	 * @since    1.0.0
	 * @return   true/false
	 */
	public function check_color( $value ) {

		if ( preg_match( '/^#[a-f0-9]{6}$/i', $value ) ) { // if user insert a HEX color with #
			return true;
		}

		return false;

	}

}
