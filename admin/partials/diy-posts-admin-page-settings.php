<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://sethalling.com/
 * @since      1.0.0
 *
 * @package    DIY_Posts
 * @subpackage DIY_Posts/admin/partials
 */
?>

<div class="wrap">

	<h1><?php echo esc_html( get_admin_page_title() ); ?></h1>

	<form method="post" action="options.php">

		<?php
		settings_fields( $this->plugin_name . '-settings' );
		do_settings_sections( $this->plugin_name );
		submit_button( 'Save Settings' );
		?>

		<div class="spinner" style="float: none;padding: 0 0 4px; margin: 0;"></div>

	</form>

</div>

<?php
