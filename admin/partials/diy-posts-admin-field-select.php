<?php

/**
 * Provide the markup for a select field
 *
 * @link       https://sethalling.com/
 * @since      1.0.0
 *
 * @package    DIY_Posts
 * @subpackage DIY_Posts/admin/partials
 */

// $atts = array(
// 	'id',
// 	'name',
// 	'label',
// 	'class',
// 	'aria',
// 	'blank',
// 	'options'
// 	'value',
// 	'description',
// );

$atts['name'] = ( ! empty( $atts['name'] ) ) ? $atts['name'] : $atts['id'];

if ( ! empty( $atts['label'] ) ) { ?>

	<label for="<?php echo esc_attr( $atts['id'] ); ?>"><?php esc_html_e( $atts['label'], $this->plugin_name ); ?></label>

<?php } ?>

<select id="<?php echo esc_attr( $atts['id'] ); ?>" class="<?php echo esc_attr( $atts['class'] ); ?>" name="<?php echo esc_attr( $atts['name'] ); ?>" aria-label="<?php esc_attr( _e( $atts['aria'], $this->plugin_name ) ); ?>">

	<?php if ( ! empty( $atts['blank'] ) ) { ?>

		<option value><?php esc_html_e( $atts['blank'], $this->plugin_name ); ?></option>

	<?php }

	if ( ! empty( $atts['options'] ) ) {

		foreach ( $atts['options'] as $option ) {

			if ( is_array( $option ) ) {

				$label = $option['label'];
				$value = $option['value'];

			} else {

				$label = strtolower( $option );
				$value = strtolower( $option );

			} ?>

			<option value="<?php echo esc_attr( $value ); ?>" <?php
				selected( $atts['value'], $value ); ?>><?php esc_html_e( $label, $this->plugin_name ); ?></option>

		<?php }

	} ?>

</select>

<?php if ( ! empty( $atts['description'] ) ) { ?>

	<span class="description"><?php esc_html_e( $atts['description'], $this->plugin_name ); ?></span>

<?php }
