<?php

/**
 * Provide the markup for a text field
 *
 * @link       https://sethalling.com/
 * @since      1.0.0
 *
 * @package    DIY_Posts
 * @subpackage DIY_Posts/admin/partials
 *
 */

// $atts = array(
// 	'id',
// 	'name',
// 	'label',
// 	'class',
// 	'type',
// 	'placeholder',
// 	'value',
// 	'description',
// );

$atts['type'] = ( ! empty( $atts['type'] ) ) ? $atts['type'] : 'text';
$atts['name'] = ( ! empty( $atts['name'] ) ) ? $atts['name'] : $atts['id'];

if ( ! empty( $atts['label'] ) ) { ?>

	<label for="<?php echo esc_attr( $atts['id'] ); ?>"><?php esc_html_e( $atts['label'], $this->plugin_name ); ?></label>

<?php } ?>

<input id="<?php echo esc_attr( $atts['id'] ); ?>" class="<?php echo esc_attr( $atts['class'] ); ?>" name="<?php echo esc_attr( $atts['name'] ); ?>" type="<?php echo esc_attr( $atts['type'] ); ?>" placeholder="<?php echo esc_attr( $atts['placeholder'] ); ?>" value="<?php echo esc_attr( $atts['value'] ); ?>">

<?php if ( ! empty( $atts['description'] ) ) { ?>

	<span class="description"><?php esc_html_e( $atts['description'], $this->plugin_name ); ?></span>

<?php }
