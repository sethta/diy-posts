<?php

/**
 * Provide the view for a metabox
 *
 * @link       https://sethalling.com/
 * @since      1.0.0
 *
 * @package    DIY_Posts
 * @subpackage DIY_Posts/admin/partials
 */

wp_nonce_field( $this->plugin_name, 'post_diy_info' );

// Difficulty select field
$atts = array(
	'id'          => 'post-diy-difficulty',
	'label'       => __( 'Difficulty', $this->plugin_name ),
	'class'       => 'widefat',
	'blank'       => 'No Difficulty Selected',
	'options'     => array(
		array(
			'label' => __( 'Easy', $this->plugin_name ),
			'value' => __( 'Easy', $this->plugin_name ),
		),
		array(
			'label' => __( 'Intermediate', $this->plugin_name ),
			'value' => __( 'Intermediate', $this->plugin_name ),
		),
		array(
			'label' => __( 'Difficult', $this->plugin_name ),
			'value' => __( 'Difficult', $this->plugin_name ),
		),
	),
);

if ( ! empty( $this->meta[ $atts['id'] ][0] ) ) {

	$atts['value'] = $this->meta[ $atts['id'] ][0];

}

apply_filters( $this->plugin_name . '-field-' . $atts['id'], $atts );

?>

<p class="<?php echo $atts['id']; ?>"><?php include( plugin_dir_path( __FILE__ ) . $this->plugin_name . '-admin-field-select.php' ); ?></p>

<?php
// Time estimate test field
$atts = array(
	'id'          => 'post-diy-time-estimate',
	'label'       => __( 'Time Estimate', $this->plugin_name ),
	'class'       => 'widefat',
	'placeholder' => 'ex: About 4 hours',
);

if ( ! empty( $this->meta[ $atts['id'] ][0] ) ) {

	$atts['value'] = $this->meta[ $atts['id'] ][0];

}

apply_filters( $this->plugin_name . '-field-' . $atts['id'], $atts );

?>

<p class="<?php echo $atts['id']; ?>"><?php include( plugin_dir_path( __FILE__ ) . $this->plugin_name . '-admin-field-text.php' ); ?></p>

<?php
// Materials cost text field
$atts = array(
	'id'          => 'post-diy-materials-cost',
	'label'       => __( 'Materials Cost', $this->plugin_name ),
	'class'       => 'widefat',
	'placeholder' => 'ex: $100-300',
);

if ( ! empty( $this->meta[ $atts['id'] ][0] ) ) {

	$atts['value'] = $this->meta[ $atts['id'] ][0];

}

apply_filters( $this->plugin_name . '-field-' . $atts['id'], $atts );

?>

<p class="<?php echo $atts['id']; ?>"><?php include( plugin_dir_path( __FILE__ ) . $this->plugin_name . '-admin-field-text.php' ); ?></p>

<?php
