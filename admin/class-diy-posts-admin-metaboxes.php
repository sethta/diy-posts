<?php

/**
 * The metabox-specific functionality of the plugin.
 *
 * @link       https://sethalling.com/
 * @since      1.0.0
 *
 * @package    DIY_Posts
 * @subpackage DIY_Posts/admin
 */

/**
 * The metabox-specific functionality of the plugin.
 *
 * @package    DIY_Posts
 * @subpackage DIY_Posts/admin
 * @author     Seth Alling <seth@sethalling.com>
 */
class DIY_Posts_Admin_Metaboxes {

	/**
	 * The post meta data
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string   $meta   The post meta data.
	 */
	private $meta;

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param    string    $plugin_name    The name of this plugin.
	 * @param    string    $version        The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

		$this->get_meta();

	}

	/**
	 * Gets the class variable $meta
	 *
	 * @since    1.0.0
	 * @access   public
	 */
	public function get_meta() {

		global $post;

		if ( empty( $post ) ) {
			if ( isset( $_GET['post'] ) ) {
				$post_id = $_GET['post'];
			} else {
				return;
			}
		} else {
			$post_id = $post->ID;
		}

		$this->meta = get_post_custom( $post_id );

	}

	/**
	 * Register the metaboxes.
	 *
	 * @since    1.0.0
	 * @access   public
	 */
	public function add_metaboxes() {

		// add_meta_box( $id, $title, $callback, $screen = null, $context = 'advanced', $priority = 'default', $callback_args = null )

		add_meta_box(
			'diy_posts_post_diy_info',
			apply_filters( $this->plugin_name . '-metabox-title-diy-info', esc_html__( 'DIY Info', $this->plugin_name ) ),
			array( $this, 'metabox' ),
			'post',
			'side',
			'high',
			array(
				'file' => 'post-diy-info'
			)
		);

	}

	/**
	 * Returns an array of the all the metabox fields and their respective types
	 *
	 * @since    1.0.0
	 * @access   public
	 * @return   array   Metabox fields and types
	 */
	private function get_metabox_fields() {

		$fields = array(
			array( 'post-diy-difficulty', 'select' ),
			array( 'post-diy-time-estimate', 'text' ),
			array( 'post-diy-materials-cost', 'text' ),
		);

		return $fields;

	}

	/**
	 * Calls a metabox file specified in the add_meta_box args.
	 *
	 * @since 	1.0.0
	 * @access 	public
	 * @return 	void
	 */
	public function metabox( $post, $params ) {

		if ( ! is_admin() ) {
			return;
		}

		include( plugin_dir_path( __FILE__ ) . 'partials/diy-posts-admin-metabox-' . $params['args']['file'] . '.php' );

	}

	/**
	 * Saves metabox data
	 *
	 * @since 	1.0.0
	 * @access 	public
	 * @param 	int      $post_id   The post ID
	 * @param 	object   $object    The post object
	 * @return 	void
	 */
	public function validate_meta( $post_id, $object ) {

		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return $post_id;
		}

		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return $post_id;
		}

		$nonce_check = $this->check_nonces( $_POST );

		if ( 0 < $nonce_check ) {
			return $post_id;
		}

		$metas = $this->get_metabox_fields();

		foreach ( $metas as $meta ) {

			$name = $meta[0];
			$type = $meta[1];

			if ( 'text' == $type ) {
				$new_value = sanitize_text_field( $_POST[ $name ] );
			} else {
				$new_value = $_POST[ $name ];
			}

			update_post_meta( $post_id, $name, $new_value );

		}

	}

	/**
	 * Check each nonce. If any don't verify, $nonce_check is increased.
	 * If all nonces verify, returns 0.
	 *
	 * @since    1.0.0
	 * @access   public
	 * @return   int   The value of $nonce_check
	 */
	private function check_nonces( $posted ) {

		$nonces = array(
			'post_diy_info',
		);
		$nonce_check = 0;

		foreach ( $nonces as $nonce ) {

			if ( ! isset( $posted[ $nonce ] ) ) {
				$nonce_check++;
			}

			if ( isset( $posted[$nonce] ) && ! wp_verify_nonce( $posted[ $nonce ], $this->plugin_name ) ) {
				$nonce_check++;
			}

		}

		return $nonce_check;

	}

}
