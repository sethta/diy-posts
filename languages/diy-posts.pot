# Copyright (C) 2017 DIY Posts
# This file is distributed under the same license as the DIY Posts package.
msgid ""
msgstr ""
"Project-Id-Version: DIY Posts 1.0.0\n"
"Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/diy-posts\n"
"POT-Creation-Date: 2017-10-24 13:08:17+00:00\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2017-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"

#: admin/class-diy-posts-admin-metaboxes.php:101
msgid "DIY Info"
msgstr ""

#: admin/class-diy-posts-admin.php:115
msgid "DIY Posts Settings"
msgstr ""

#. #-#-#-#-#  diy-posts.pot (DIY Posts 1.0.0)  #-#-#-#-#
#. Plugin Name of the plugin/theme
#: admin/class-diy-posts-admin.php:116
msgid "DIY Posts"
msgstr ""

#: admin/class-diy-posts-admin.php:148
msgid "Background Color"
msgstr ""

#: admin/partials/diy-posts-admin-metabox-post-diy-info.php:18
#: public/partials/diy-posts-public-display.php:20
msgid "Difficulty"
msgstr ""

#: admin/partials/diy-posts-admin-metabox-post-diy-info.php:23
#: admin/partials/diy-posts-admin-metabox-post-diy-info.php:24
msgid "Easy"
msgstr ""

#: admin/partials/diy-posts-admin-metabox-post-diy-info.php:27
#: admin/partials/diy-posts-admin-metabox-post-diy-info.php:28
msgid "Intermediate"
msgstr ""

#: admin/partials/diy-posts-admin-metabox-post-diy-info.php:31
#: admin/partials/diy-posts-admin-metabox-post-diy-info.php:32
msgid "Difficult"
msgstr ""

#: admin/partials/diy-posts-admin-metabox-post-diy-info.php:53
#: public/partials/diy-posts-public-display.php:24
msgid "Time Estimate"
msgstr ""

#: admin/partials/diy-posts-admin-metabox-post-diy-info.php:74
#: public/partials/diy-posts-public-display.php:28
msgid "Materials Cost"
msgstr ""

#. Description of the plugin/theme
msgid "An easy interface to classify your posts by difficulty, time and cost"
msgstr ""

#. Author of the plugin/theme
msgid "Seth Alling"
msgstr ""

#. Author URI of the plugin/theme
msgid "https://sethalling.com/"
msgstr ""
