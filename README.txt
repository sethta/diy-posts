=== Plugin Name ===
Contributors: sethta
Donate link: https://sethalling.com/
Tags: diy, posts
Requires at least: 4.8
Tested up to: 4.8.2
Stable tag: 1.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

DIY Posts helps you classify your posts by difficulty, time and cost.

== Changelog ==

= 1.0 =
* Initial plugin launch