<?php

/**
 * @link              https://sethalling.com/
 * @since             1.0.0
 * @package           DIY_Posts
 *
 * @wordpress-plugin
 * Plugin Name:       DIY Posts
 * Description:       An easy interface to classify your posts by difficulty, time and cost
 * Version:           1.0.0
 * Requires at least: 4.8
 * Tested up to:      4.8
 * Author:            Seth Alling
 * Author URI:        https://sethalling.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       diy-posts
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	die;
}

// Plugin Constants
if ( ! defined( 'DIY_POSTS_VERSION' ) ) {
	define( 'DIY_POSTS_VERSION', '1.0.0' );
}
if ( ! defined( 'DIY_POSTS_WP_BASENAME' ) ) {
	define( 'DIY_POSTS_WP_BASENAME', plugin_basename( __FILE__ ) );
}
if ( ! defined( 'DIY_POSTS_WP_NAME' ) ) {
	define( 'DIY_POSTS_WP_NAME', trim( dirname( DIY_POSTS_WP_BASENAME ), '/' ) );
}
if ( ! defined( 'DIY_POSTS_WP_DIR' ) ) {
	define( 'DIY_POSTS_WP_DIR', WP_PLUGIN_DIR . '/' . DIY_POSTS_WP_NAME );
}
if ( ! defined( 'DIY_POSTS_WP_URL' ) ) {
	define( 'DIY_POSTS_WP_URL', WP_PLUGIN_URL . '/' . DIY_POSTS_WP_NAME );
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-diy-posts-activator.php
 */
function activate_diy_posts() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-diy-posts-activator.php';
	DIY_Posts_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-diy-posts-deactivator.php
 */
function deactivate_diy_posts() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-diy-posts-deactivator.php';
	DIY_Posts_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_diy_posts' );
register_deactivation_hook( __FILE__, 'deactivate_diy_posts' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-diy-posts.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_diy_posts() {

	$plugin = new DIY_Posts();
	$plugin->run();

}
run_diy_posts();
