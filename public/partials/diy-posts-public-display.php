<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://sethalling.com/
 * @since      1.0.0
 *
 * @package    DIY_Posts
 * @subpackage DIY_Posts/public/partials
 */

$background = ( ! empty( $this->options['background-color'] ) && '#cccccc' != $this->options['background-color'] ) ? ' style="background-color: ' . $this->options['background-color'] . ';"' : null;

$new_content = '<div class="diy-info"' . $background . '>';

if ( ! empty( $meta['post-diy-difficulty'][0] ) ) {
	$new_content .= '<div class="diy-difficulty"><strong>' . __( 'Difficulty', $this->plugin_name ) . ':</strong> ' . $meta['post-diy-difficulty'][0] . '</div>';
}

if ( ! empty( $meta['post-diy-time-estimate'][0] ) ) {
	$new_content .= '<div class="diy-time-estimate"><strong>' . __( 'Time Estimate', $this->plugin_name ) . ':</strong> ' . $meta['post-diy-time-estimate'][0] . '</div>';
}

if ( ! empty( $meta['post-diy-materials-cost'][0] ) ) {
	$new_content .= '<div class="diy-materials-cost"><strong>' . __( 'Materials Cost', $this->plugin_name ) . ':</strong> ' . $meta['post-diy-materials-cost'][0] . '</div>';
}

$new_content .= '</div>';
