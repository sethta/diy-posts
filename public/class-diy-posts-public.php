<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://sethalling.com/
 * @since      1.0.0
 *
 * @package    DIY_Posts
 * @subpackage DIY_Posts/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    DIY_Posts
 * @subpackage DIY_Posts/public
 * @author     Seth Alling <seth@sethalling.com>
 */
class DIY_Posts_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * The plugin options.
	 *
	 * @since   1.0.0
	 * @access   private
	 * @var   string   	$options    The plugin options.
	 */
	private $options;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

		$this->get_options();

	}

	/**
	 * Gets the class variable $options
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function get_options() {

		$this->options = get_option( $this->plugin_name . '-settings' );

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/diy-posts-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Gets the post's meta
	 *
	 * @since    1.0.0
	 * @return   array   Array of post's metadata
	 */
	public function get_meta( $id ) {

		if ( ! empty( $id ) ) {
			return get_post_custom( $id );
		}

	}

	/**
	 * Display the DIY post info above content if exists
	 *
	 * @since    1.0.0
	 */
	public function display_diy_posts_info( $content ) {

		global $post;

		$meta = $this->get_meta( $post->ID );

		// Make sure at least one info meta exists
		$i = 0;
		$meta_fields = array(
			'post-diy-difficulty',
			'post-diy-materials-cost',
			'post-diy-time-estimate',
		);
		foreach ( $meta_fields as $meta_field ) {

			if ( ! empty( $meta[ $meta_field ][0] ) ) {
				++$i;
				break;
			}

		}

		if ( $i != 0 ) {

			include( plugin_dir_path( __FILE__ ) . 'partials/diy-posts-public-display.php' );

			$new_content .= $content;

			return $new_content;

		}

		return $content;

	}

}
